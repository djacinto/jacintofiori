sap.ui.define([
	"sap/ui/base/Object"
], function(Object) {
	"use strict";

	var services = {
		url: null,
		oModel : null,
		setUrl: function(Url){
			this.url = Url;
			this.oModel = new sap.ui.model.odata.ODataModel(this.url, true);
		},
		consumeModel: function(params, successCb, errorCb, urlParametersX) {
			this.oModel.read(params, {
				urlParameters: urlParametersX,
				async: false,
				success: function(oData, oResponse) {
					successCb(oData, oResponse);
				},
				error: function(err) {
						errorCb(err);
					}
			});
		},
		creteModel: function(params, object, successCb, errorCb, urlParameters) {
			this.oModel.create(params, object, {
				success: function(oData, oResponse) {
					successCb(oData, oResponse);
				},
				error: function(err) {
					errorCb(err);
				}
			});
		},
		updateModel: function(params, object, successCb, errorCb, urlParameters) {
			this.oModel.update(params, object, {
				method: "PUT",
				success: function(oData, oResponse) {
					successCb(oData, oResponse);
				},
				error: function(err) {
					errorCb(err);
				}
			});
		},
		deleteModel: function(params, object, successCb, errorCb, urlParameters){
			this.oModel.delete(params, {
				success: function(oData, oResponse) {
					successCb(oData, oResponse);
				},
				error: function(err) {
					errorCb(err);
				}
			});
		}

	};
	return services;
});