sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel",
	"odata/testeOdata/utils/apiConnetor2"
], function (Controller, History, JSONModel, apiConnetor) {
	"use strict";

	return Controller.extend("odata.testeOdata.controller.DetalheProduto", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf odata.testeOdata.view.DetalheProduto
		 * 
		 * RECEITA de BOLO
		 * 2 colheres de sopa de olho
		 * 
		 * 
		 */
		invoicePath: null,
		url: null,
		oModel: null,
		onInit: function () {
			// this.url = this.getOwnerComponent().getModel().sServiceUrl;
			apiConnetor.setUrl(this.getOwnerComponent().getModel().sServiceUrl);
			this.getOwnerComponent()._oRouter.getTarget("DetalheProduto").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},
		pressButomCreate: function (oEvent) {
			var object = this.getView().getModel().getData();
			this.oModel.create("/Products", object, {
				success: function (oData, oResponse) {

				},
				error: function (err) {

				}
			});

		},
		
		pressButomUpdate: function(oEvent){
			var object = this.getView().getModel("Produto").getData();
			delete object["ToConversionFactors"];
			delete object["ToSupplier"];
			delete object["__metadata"];
			var path = "/" + this.invoicePath;
			apiConnetor.updateModel(
					path,
					object,
					function (oData, Response) {

					},
					function (err) {

					}, {}
				);
			
		},
		handleRouteMatched: function (oEvent) {
			if (oEvent.getParameters().data.invoicePath !== undefined) {
				this.invoicePath = oEvent.getParameters().data.invoicePath;
				var path = "/" + this.invoicePath;
				var me = this;
				apiConnetor.consumeModel(
					path,
					function (oData, Response) {

						var json = new JSONModel();
						json.setData(oData);
						me.getView().setModel(json, "Produto");

					},
					function (err) {

					}, {
						"$expand": "ToConversionFactors,ToSupplier"
					}
				);
			}
		},
		onBack: function () {
			this.getOwnerComponent()._oRouter.navTo("TargetMainPage", {}, false);
			// this.getOwnerComponent()._oRouter.navBack();
			// var sPreviousHash = History.getInstance().getPreviousHash();

			// 	if (sPreviousHash !== undefined) {
			// 		// The history contains a previous entry
			// 		history.go(-1);
			// 	}
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf odata.testeOdata.view.DetalheProduto
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf odata.testeOdata.view.DetalheProduto
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf odata.testeOdata.view.DetalheProduto
		 */
		//	onExit: function() {
		//
		//	}

	});

});