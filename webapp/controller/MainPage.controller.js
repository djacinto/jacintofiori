sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter"
], function (Controller, FilterOperator, Filter) {
	"use strict";

	return Controller.extend("odata.testeOdata.controller.MainPage", {
		jacinto: "Danilo",

		onInit: function () {

		},
		onLiveChange: function (oEvent) {

		},
		onPressItemList: function (oEvent) {
			var path = oEvent.getSource().getBindingContext();
			var contexto = path.sPath.slice(1, path.sPath.length);
			this.getOwnerComponent()._oRouter.navTo("DetalheProduto", {
				invoicePath: contexto // numero da Nota
			}, true);
		},
		onSearch: function (oEvent) {
			var sQuery = oEvent.getSource().getValue();
			var aFilters = [];

			if (sQuery && sQuery.length > 0) {
				var filter = new Filter("SupplierName", FilterOperator.Contains, sQuery);
				aFilters.push(filter);
			}

			// update list binding
			var list = this.byId("list");
			var binding = list.getBinding("items");
			binding.filter(aFilters, "Application");

		}
	});
});